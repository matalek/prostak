/* Zadanie 6.
 * tk334580 & am347171
 *
 * Instrukcja - klasa ABC
 */
 
#ifndef INSTRUKCJA_H
#define INSTRUKCJA_H

#include "cpu.h"
#include "ram.h"
#include "exceptions.h"

class Instrukcja {

    public:
        virtual ~Instrukcja() { }
        
        virtual void execute(CPU& cpu, RAM& ram) const = 0;
        
    protected:
        // wczytuje znak R, a potem liczbę którą zwraca
        int readRegister(std::istringstream& line_stream) {
            char c;
            line_stream >> c;
            if (c == 'R')
                return readValue(line_stream);
            else
                throw ParserException{};
        }

        // wczytuje znak M, a potem liczbę którą zwraca
        int readAddress(std::istringstream& line_stream) {
            char c;
            line_stream >> c;
            if (c == 'M')
                return readValue(line_stream);
            else
                throw ParserException{};
        }

        // wczytuje liczbę, którą potem zwraca
        int readValue(std::istringstream& line_stream) {
            std::string s;
            line_stream >> s;

            // pomocniczy wskaźnik do sprawdzenia, czy poza liczbą nic nie było
            std::size_t* pos = new size_t();
            int val;
            try {
                val = std::stoi(s, pos);
            } catch(std::exception) {
                delete pos;
                throw ParserException{};
            }
            
            size_t end = *pos;
            delete pos;

            // po liczbie coś jeszcze występowało
            if (end != s.size())
                throw ParserException{};

            return val;
        }
};

#endif // INSTRUKCJA_H
