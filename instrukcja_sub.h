/* Zadanie 6.
 * tk334580 & am347171
 *
 * InstrukcjaSub
 */

#ifndef INSTRUKCJA_SUB_H
#define INSTRUKCJA_SUB_H

#include <sstream>

#include "cpu.h"
#include "ram.h"
#include "instrukcja.h"

class InstrukcjaSub : public Instrukcja {

    public:
        InstrukcjaSub(std::istringstream& line_stream) {
            r1 = readRegister(line_stream);
            r2 = readRegister(line_stream);
        }

        void execute(CPU& cpu, RAM&) const {
            int32_t new_value = cpu.getValue(r1) - cpu.getValue(r2);
            cpu.setValue(r1, new_value);
        }

    private:
        register_type r1;
        register_type r2;
};

#endif // INSTRUKCJA_SUB_H
