/* Zadanie 6.
 * tk334580 & am347171
 *
 * SchedulingAlgorithm - klasa ABC
 */

#ifndef SCHEDULING_ALGORITHM
#define SCHEDULING_ALGORITHM

#include <vector>
#include "proces.h"

using time_type = unsigned int;

class SchedulingAlgorithm {

	public:

		virtual ~SchedulingAlgorithm() { };

		// wczytuje wektor procesów i na jego podstawie ustala
		// kolejność wykonywania
		virtual void setProcesses(std::vector<Proces>& v) = 0;

		// zwraca parę (nr_procesu, ilość_czasu), które wskazuje,
		// że proces o numerze _nr_procesu powinien wykonywać się
		// przez ilość_czasu. Jeśli nr_procesu = -1, to wszystkie
		// procesy wykonały się w całości
		virtual std::pair<int, time_type> next() = 0;

};


#endif // SCHEDULING_ALGORITHM
