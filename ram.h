/* Zadanie 6.
 * tk334580 & am347171
 *
 * RAM
 */

#ifndef RAM_H
#define RAM_H

#include <stdint.h>
#include <vector>

#include "exceptions.h"

using memory_type = int;

class RAM {

    public:
        void setSize(memory_type N) {
            if (N <= 0)
                throw IllegalArgumentException{};
            memory.resize(N);
        }

        memory_type getSize() const {
            return memory.size();
        }

        void clean() {
            fill(memory.begin(), memory.end(), 0);
        }
        
        int32_t getValue(memory_type adr) const {
            if (adr < 0 || static_cast<unsigned int>(adr) >= memory.size())
                throw InvalidAddressException{};
            return memory[adr];
        }

        void setValue(memory_type adr, int32_t val) {
            if (adr < 0 || static_cast<unsigned int>(adr) >= memory.size())
                throw InvalidAddressException{};
            memory[adr] = val;
        }

    private:
        std::vector<int32_t> memory;
};

#endif // RAM_H
