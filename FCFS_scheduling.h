/* Zadanie 6.
 * tk334580 & am347171
 *
 * FCFSScheduling
 * 
 * Procesy są wykonywane w kolejności uruchamiania.
 */

#ifndef FCFS_SCHEDULING_H
#define FCFS_SCHEDULING_H

#include <queue>

#include "scheduling_algorithm.h"

class FCFSScheduling : public SchedulingAlgorithm {
    
    public:
        void setProcesses(std::vector<Proces>& v) {
            // czyszczenie procesów z poprzedniego wykonania
            while (!processes_in_order.empty())
                processes_in_order.pop();
            
            for (size_t i = 0; i < v.size(); ++i)
                processes_in_order.push(std::make_pair(i, v[i].getNumber()));
        }
        
        std::pair<int, time_type> next() {
            if (processes_in_order.empty())
                return std::make_pair(-1,0);
            
            std::pair<int, time_type> res = processes_in_order.front();
            processes_in_order.pop();

            return res;
        }

    private:
        std::queue<std::pair<int, time_type> > processes_in_order;
};

#endif // FCFS_SCHEDULING_H
