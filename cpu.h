/* Zadanie 6.
 * tk334580 & am347171
 *
 * CPU
 */

#ifndef CPU_H
#define CPU_H

#include <stdint.h>
#include <vector>

#include "exceptions.h"

using register_type = int;

class CPU {

    public:
        void setSize(register_type N) {
            if (N <= 0)
                throw IllegalArgumentException{};
            registers.resize(N);
        }

        register_type getSize() const {
            return registers.size();
        }

        void clean() {
            fill(registers.begin(), registers.end(), 0);
        }
        
        int32_t getValue(register_type r) const {
            if (r <= 0 || static_cast<unsigned int>(r) > registers.size())
                throw InvalidRegisterException{};
            return registers[r - 1];
        }

        void setValue(register_type r, int32_t val) {
            if (r <= 0 || static_cast<unsigned int>(r) > registers.size())
                throw InvalidRegisterException{};
            registers[r - 1] = val;
        }

    private:
        std::vector<int32_t> registers;
};

#endif // CPU_H
