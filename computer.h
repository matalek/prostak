/* Zadanie 6.
 * tk334580 & am347171
 *
 * Computer
 *
 * Przyjęta koncepcja / przyjęte założenia:
 *
 * - Komputer nie przechowuje systemów operacyjnych, które zostały
 * na nim zainstalowane, gdyż na każdym komputerze może zostać
 * zainstalowane wiele systemów operacyjnych, a pamiętanie ich
 * niczemu nie służy.
 *
 * - W związku z powyższym, konstruktor kopiujący oraz operator
 * przypisania klasy komputer kopiują (głęboko) CPU i RAM, ale nie
 * zainstalowane systemy operacyjne. Na skopiowanym komputerze w związku
 * z tym można zmieniać rozmiar CPU i RAM.
 *
 * - Kopiowanie systemu jest kopiowaniem płytkim - wszak systemu nie
 * jest właścicielem CPU ani RAM.
 *
 * - Implementacje metod zostały umieszczone w plikach .h, gdyż
 * do tej pory duża część zadań była do oddania właśniej w takiej
 * postaci (bez plików .cc), a implementowane funkcje zazwyczaj
 * nie mają dużej objętości.
 */
 
#include <iostream>
#include <memory>

#include "cpu.h"
#include "ram.h"
#include "os.h"
#include "algorithms.h"

#ifndef COMPUTER_H
#define COMPUTER_H

class Computer {

    public:
        Computer() : is_installed(false) {
            cpu = std::make_shared<CPU>();
            ram = std::make_shared<RAM>();
        }
        
        Computer(const Computer& other) : is_installed(false) {
            cpu = std::make_shared<CPU>(*other.cpu);
            ram = std::make_shared<RAM>(*other.ram);
        }

        Computer& operator=(const Computer& other) {
            cpu = std::make_shared<CPU>(*other.cpu);
            ram = std::make_shared<RAM>(*other.ram);
            is_installed = false;
            return *this;
        }

        /* ustawia procesor z podaną liczbą rejestrów. Liczba rejestrów
         * musi być dodatnia, wpp. zgłasza wyjątek IllegalArgumentException. */
        void setCPU(register_type numOfRegisters) {
            // jeśli już zainstalowano system, zgłaszamy błąd
            if (is_installed)
                throw IllegalChangeException{};
            cpu->setSize(numOfRegisters);
        }

        /* ustawia rozmiar dostępnej pamięci RAM w komputerze. Rozmiar
         * pamięci musi być dodatni, wpp. zgłasza wyjątek
         * IllegalArgumentException. */
        void setRAM(memory_type size) {
            // jeśli już zainstalowano system, zgłaszamy błąd
            if (is_installed)
                throw IllegalChangeException{};
            ram->setSize(size);
        }

        /* instaluje nowy system operacyjny na komputerze z wybranym
         * algorytmem szeregowania zadań. W przypadku braku
         * skonfigurowanego procesora lub pamięci zgłasza wyjątki
         * odpowiednio NoCPUException lub NoRAMException. */
        std::shared_ptr<OS> installOS(std::shared_ptr<SchedulingAlgorithm> alg) {
            if (!cpu->getSize())
                throw NoCPUException{};
            if (!ram->getSize())
                throw NoRAMException{};

            // czyszczenie rejestrów i pamięci podczas instalacji systemu
            cpu->clean();
            ram->clean();

            is_installed = true;
            return std::make_shared<OS>(OS{cpu, ram, alg});
        }

    private:
        std::shared_ptr<CPU> cpu;
        std::shared_ptr<RAM> ram;
        bool is_installed; // czy zainstalowano już system operacyjny
};

// tworzy algorytm szeregowania First Come First Served (FCFS)
std::shared_ptr<SchedulingAlgorithm> createFCFSScheduling() {
    return std::make_shared<FCFSScheduling>(FCFSScheduling{});
}

// tworzy algorytm szeregowania Round Robin z podanym kwantem czasu
std::shared_ptr<SchedulingAlgorithm> createRRScheduling(time_type quantum) {
    return std::make_shared<RRScheduling>(RRScheduling{quantum});
}

// tworzy algorytm szeregowania Shorted Job First
std::shared_ptr<SchedulingAlgorithm> createSJFScheduling() {
    return std::make_shared<SJFScheduling>(SJFScheduling{});
}

#endif // COMPUTER_H
