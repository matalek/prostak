/* Zadanie 6.
 * tk334580 & am347171
 *
 * OS
 */

#ifndef OS_H
#define OS_H

#include <vector>
#include <list>
#include <memory>

#include "proces.h"
#include "scheduling_algorithm.h"

class OS {

    public:
        OS(std::shared_ptr<CPU>& cpu, std::shared_ptr<RAM>& ram, std::shared_ptr<SchedulingAlgorithm>& alg)
            : cpu(cpu), ram(ram), alg(alg) { }
            
        /* wykonuje program napisany w języku PROSTAK. Procesy są
         * szeregowane zgodnie z algorytmem wybranym przy instalacji
         * systemu operacyjnego. */ 
        void executePrograms(std::list<std::string> const& programs) {
            std::vector<Proces> processes;
            for (auto program : programs)
                processes.push_back(Proces{program});

            // ustawianie procesów w kolejności według zadanego algorytmu
            alg->setProcesses(processes);

            while (true) {
                // przydzielanie czasu kolejnym procesom
                std::pair<int, time_type> next_process = alg->next();
                if (next_process.first == -1)
                    break;
                processes[next_process.first].execute(*cpu, *ram, next_process.second);
            }
        }

    private:
        std::shared_ptr<CPU> cpu;
        std::shared_ptr<RAM> ram;
        std::shared_ptr<SchedulingAlgorithm> alg;
};

#endif // OS_H
