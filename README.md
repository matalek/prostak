## Zadanie 6. ##
### Języki i narzędzia programowania I ###
### Wydział Matematyki, Informatyki i Mechaniki Uniwersytetu Warszawskiego ###

Celem zadania jest napisanie prostego symulatora komputera z zainstalowanym systemem
operacyjnym wspierającym szeregowanie procesów. System operacyjny potrafi wykonywać
programy napisane w języku maszynowym PROSTAK zbliżonym do asemblera.

Komputer ma procesor z konfigurowalną liczbą rejestrów oraz konfigurowalną
pamięć RAM. Rejestry przechowują 4-bajtowe wartości liczbowe, na których mogą
być wykonywane operacje arytmetyczne. Rejestry są numerowane od jednego.
Pamięć RAM to ciąg komórek liczbowych o wielkości 4 bajty każda. Pamięć
adresowana jest od zera.

Na komputerze można zainstalować system operacyjny z planistą odpowiedzialnym
za szeregowanie procesów. Komputer musi mieć niezerową liczbę rejestrów
oraz niezerową pamięć, aby można było zainstalować system operacyjny.
System operacyjny umożliwia wykonywanie programów napisanych w języku
maszynowym PROSTAK. Przy instalacji systemu operacyjnego rejestry i pamięć
są inicjowane na zero. Rejestry oraz pamięć są współdzielone przez wszystkie procesy.

Wspierane algorytmy szeregowania procesów to:
    * First Come First Served
            Procesy (tj. wykonujące się programy) są wykonywane w kolejności
            uruchamiania.
    * Round Robin
            Każdy proces w systemie dostaje ten sam kwant czasu, po wyczerpaniu
            którego jest wywłaszczany i następny proces uzyskuje dostęp do procesora.
            Czas w naszym systemie jest liczony taktami procesora. Każda instrukcja
            maszynowa zajmuje 1 takt procesora.
    * Shortest Job First
            Do wykonania wybierany jest proces, który najszybciej się skończy.

Program w języku PROSTAK to ciąg instrukcji oddzielonych znakiem nowej linii.
Dopuszczalne instrukcje w języku PROSTAK to:
    * SET RNUM VAL
            Ustawia wartość rejestru nr NUM na wartość liczbową VAL.
            Przykład: SET R1 123
    * LOAD RNUM MADDR
            Ładuje wartość komórki pamięci spod adresu ADDR do rejestru nr NUM.
            Przykład: LOAD R1 M42
    * STORE MADDR RNUM
            Zapisuje wartość z rejestru nr NUM do komórki pamięci pod adresem ADDR.
            Przykład: STORE M42 R1
    * ADD RNUM1 RNUM2
            Dodaje wartość rejestru nr NUM2 do wartości rejestru nr NUM1,
            wynik przechowywany jest w rejestrze NUM1.
            Przykład: ADD R1 R2
    * SUB RNUM1 RNUM2
            Odejmuje wartość rejestru nr NUM2 od wartości rejestru nr NUM1,
            wynik przechowywany jest w rejestrze NUM1.
            Przykład: SUB R1 R2
    * MUL RNUM1 RNUM2
            Mnoży wartość rejestru nr NUM1 przez wartość rejestru nr NUM2,
            wynik przechowywany jest w rejestrze NUM1.
            Przykład: MUL R1 R2
    * DIV RNUM1 RNUM2
            Dzieli wartość rejestru nr NUM1 przez wartość rejestru nr NUM2,
            wynik przechowywany jest w rejestrze NUM1.
            Przykład: DIV R1 R2
    * PRINTLN RNUM
            Wypisuje na standardowe wyjście wartość rejestru nr NUM
            zakończoną znakiem nowej linii.
            Przykład: PRINTLN R1

Dostarczona klasa Computer powinna mieć bezparametrowy konstruktor oraz
co najmniej następujące metody publiczne:

    * void setCPU(register_type numOfRegisters)
            Ustawia procesor z podaną liczbą rejestrów. Liczba rejestrów
            musi być dodatnia, wpp. zgłasza wyjątek IllegalArgumentException.

    * void setRAM(memory_type size)
            Ustawia rozmiar dostępnej pamięci RAM w komputerze. Rozmiar
            pamięci musi być dodatni, wpp. zgłasza wyjątek
            IllegalArgumentException.

    * std::shared_ptr<OS> installOS(std::shared_ptr<SchedulingAlgorithm> alg)
            Instaluje nowy system operacyjny na komputerze z wybranym algorytmem
            szeregowania zadań. W przypadku braku skonfigurowanego procesora
            lub pamięci zgłasza wyjątki odpowiednio NoCPUException lub NoRAMException.

Pomocnicze funkcje dla klasy Computer do tworzenia algorytmów szeregowania procesów:

    * std::shared_ptr<SchedulingAlgorithm> createFCFSScheduling()
            Tworzy algorytm szeregowania First Come First Served (FCFS).

    * std::shared_ptr<SchedulingAlgorithm> createRRScheduling(time_type quantum)
            Tworzy algorytm szeregowania Round Robin z podanym kwantem czasu.

    * std::shared_ptr<SchedulingAlgorithm> createSJFScheduling()
            Tworzy algorytm szeregowania Shorted Job First.

Dostarczona klasa OS powinna udostępniać co najmniej następujące metody publiczne:

    * void executePrograms(std::list<std::string> const& programs)
            Wykonuje program napisany w języku PROSTAK. Procesy są szeregowane
            zgodnie z algorytmem wybranym przy instalacji systemu operacyjnego.
            Może zgłosić nastepujące wyjątki:
            - UnknownInstructionException w przypadku nieznanej instrukcji,
            - InvalidRegisterException w przypadku nieprawidłowego numeru rejestru,
            - InvalidAddressException w przypadku nieprawidłowego adresu pamięci,
            - DivisionByZeroException w przypadku dzielenia przez 0.

== Rozwiązanie ==

Każda klasa w systemie powinna być zaimplementowana w osobnym pliku
z rozszerzeniem .h i ewentualnie odpowiednim pliku z rozszerzeniem .cc.
Wśród dostarczonych plików musi istnieć plik computer.h, który będzie włączany
przez programistę korzystającego z systemu. 
Nazwy klas i metod powinny być na tyle czytelne, żeby nie było potrzeby ich
komentowania. W sytuacjach, w których nie ma możliwości jasnego wyrażenia
intencji autorów za pomocą kodu, należy zamieścić komentarz.

Pliki należy umieścić w katalogu:

grupaN/zadanie6/ab123456+cd123456

lub

grupaN/zadanie6/ab123456+cd123456+ef123456

gdzie N jest numerem grupy, a ab123456, cd123456, ef123456 są
identyfikatorami członków zespołu umieszczającego to rozwiązanie.
Katalog z rozwiązaniem nie powinien zawierać innych plików, ale może
zawierać podkatalog private, gdzie można umieszczać różne pliki, np.
swoje testy. Pliki umieszczone w tym podkatalogu nie będą oceniane.
