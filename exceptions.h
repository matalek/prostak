/* Zadanie 6.
 * tk334580 & am347171
 *
 * Exceptions
 */

#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

class DivisionByZeroException : public std::exception {
    
    public:
        const char* what() const throw() {
            return "DivisionByZeroException";
        }
};

class IllegalArgumentException : public std::exception {
    
    public:
        const char* what() const throw() {
            return "IllegalArgumentException";
        }
};

class IllegalChangeException : public std::exception {
    
    public:
        const char* what() const throw() {
            return "IllegalChangeException";
        }
};

class InvalidAddressException : public std::exception {
    
    public:
        const char* what() const throw() {
            return "InvalidAddressException";
        }
};

class InvalidRegisterException : public std::exception {
    
    public:
        const char* what() const throw() {
            return "InvalidRegisterException";
        }
};

class NoCPUException : public std::exception {
    
    public:
        const char* what() const throw() {
            return "NoCPUException";
        }
};

class NoRAMException : public std::exception {
    
    public:
        const char* what() const throw() {
            return "NoRAMException";
        }
};

class ParserException : public std::exception {
    
    public:
        const char* what() const throw() {
            return "ParserException";
        }
};

class UnknownInstructionException : public std::exception {
    
    public:
        const char* what() const throw() {
            return "UnknownInstructionException";
        }
};

#endif // EXCEPTIONS_H
