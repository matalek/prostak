/* Zadanie 6.
 * tk334580 & am347171
 *
 * InstrukcjaStore
 */

#ifndef INSTRUKCJA_STORE_H
#define INSTRUKCJA_STORE_H

#include <sstream>

#include "cpu.h"
#include "ram.h"
#include "instrukcja.h"

class InstrukcjaStore : public Instrukcja {

    public:
        InstrukcjaStore(std::istringstream& line_stream) {
            adr = readAddress(line_stream);
            r = readRegister(line_stream);
        }

        void execute(CPU& cpu, RAM& ram) const {
            ram.setValue(adr, cpu.getValue(r));
        }

    private:
        memory_type adr;
        register_type r;
};

#endif // INSTRUKCJA_STORE_H

