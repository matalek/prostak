/* Zadanie 6.
 * tk334580 & am347171
 *
 * Proces
 */

#ifndef PROCES_H
#define PROCES_H

#include <queue>
#include <string>
#include <memory>
#include <sstream>

#include "cpu.h"
#include "ram.h"
#include "instrukcje.h"
#include "exceptions.h"

class Proces {

    public:
        Proces(std::string program) {
            // tworzenie kolejki instrukcji
            std::istringstream program_stream(program);
            std::string line;
            while (std::getline(program_stream, line)) {
                std::istringstream line_stream(line);

                // wczytanie polecenia
                std::string instruction_name;
                line_stream >> instruction_name;

                std::shared_ptr<Instrukcja> ptr;
                if (instruction_name == "SET")
                    ptr = std::make_shared<InstrukcjaSet>(InstrukcjaSet{line_stream});
                else if (instruction_name == "LOAD")
                    ptr = std::make_shared<InstrukcjaLoad>(InstrukcjaLoad{line_stream});
                else if (instruction_name == "STORE")
                    ptr = std::make_shared<InstrukcjaStore>(InstrukcjaStore{line_stream});
                else if (instruction_name == "ADD")
                    ptr = std::make_shared<InstrukcjaAdd>(InstrukcjaAdd{line_stream});
                else if (instruction_name == "SUB")
                    ptr = std::make_shared<InstrukcjaSub>(InstrukcjaSub{line_stream});
                else if (instruction_name == "MUL")
                    ptr = std::make_shared<InstrukcjaMul>(InstrukcjaMul{line_stream});
                else if (instruction_name == "DIV")
                    ptr = std::make_shared<InstrukcjaDiv>(InstrukcjaDiv{line_stream});
                else if (instruction_name == "PRINTLN")
                    ptr = std::make_shared<InstrukcjaPrintln>(InstrukcjaPrintln{line_stream});
                else if (instruction_name == "") {
                    // jeśli linia składa się z białych znaków, to ją ignorujemy
                    continue;
                } else
                    throw UnknownInstructionException{};

                // sprawdzanie, czy za poprawnym poleceniem nie ma śmieciowych znaków
                std::string rest = "";
                line_stream >> rest;
                if (rest != "")
                    throw ParserException{};

                // dodanie stworzonej instrukcji
                instructions.push(ptr);
            }
        }

        // zwraca liczbę instrukcji procesu
        int getNumber() const {
            return instructions.size();
        }

        // wykonuje n następnych instrukcji
        void execute(CPU& cpu, RAM& ram, size_t n) {
            // żeby nie wykonywać więcej, niż liczba pozostałych
            if (n > instructions.size())
                n = instructions.size();

            for (size_t i = 0; i < n; i++) {
                instructions.front()->execute(cpu, ram);
                instructions.pop();
            }
        }

    private:
        std::queue<std::shared_ptr<Instrukcja> > instructions;
};

#endif // PROCES_H
