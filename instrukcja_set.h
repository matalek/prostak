/* Zadanie 6.
 * tk334580 & am347171
 *
 * InstrukcjaSet
 */

#ifndef INSTRUKCJA_SET_H
#define INSTRUKCJA_SET_H

#include <sstream>

#include "cpu.h"
#include "ram.h"
#include "instrukcja.h"

class InstrukcjaSet : public Instrukcja {

    public:
        InstrukcjaSet(std::istringstream& line_stream) {
            r = readRegister(line_stream);
            val = readValue(line_stream);
        }

        void execute(CPU& cpu, RAM&) const {
            cpu.setValue(r, val);
        }

    private:
        register_type r;
        int32_t val;
};

#endif // INSTRUKCJA_SET_H
