/* Zadanie 6.
 * tk334580 & am347171
 *
 * InstrukcjaLoad
 */
 
#ifndef INSTRUKCJA_LOAD_H
#define INSTRUKCJA_LOAD_H

#include <sstream>

#include "cpu.h"
#include "ram.h"
#include "instrukcja.h"

class InstrukcjaLoad : public Instrukcja {

    public:
        InstrukcjaLoad(std::istringstream& line_stream) {
            r = readRegister(line_stream);
            adr = readAddress(line_stream);
        }

        void execute(CPU& cpu, RAM& ram) const {
            cpu.setValue(r, ram.getValue(adr));
        }

    private:
        register_type r;
        memory_type adr;
};

#endif // INSTRUKCJA_LOAD_H
