/* Zadanie 6.
 * tk334580 & am347171
 *
 * InstrukcjaDiv
 */

#ifndef INSTRUKCJA_DIV_H
#define INSTRUKCJA_DIV_H

#include <sstream>

#include "cpu.h"
#include "ram.h"
#include "instrukcja.h"
#include "exceptions.h"

class InstrukcjaDiv : public Instrukcja {

    public:
        InstrukcjaDiv(std::istringstream& line_stream) {
            r1 = readRegister(line_stream);
            r2 = readRegister(line_stream);
        }

        void execute(CPU& cpu, RAM&) const {
            int32_t val2 = cpu.getValue(r2);
            if (val2 == 0)
                throw DivisionByZeroException{};
            int32_t new_value = cpu.getValue(r1) / val2;
            cpu.setValue(r1, new_value);
        }

    private:
        register_type r1;
        register_type r2;
};

#endif // INSTRUKCJA_DIV_H
