/* Zadanie 6.
 * tk334580 & am347171
 *
 * Nagłówki wszystkich algorytmów szeregujących
 */

#include "scheduling_algorithm.h"
#include "FCFS_scheduling.h"
#include "SJF_scheduling.h"
#include "RR_scheduling.h"
