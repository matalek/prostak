/* Zadanie 6.
 * tk334580 & am347171
 *
 * Nagłówki wszystkich możliwych instrukcji
 */

#include "instrukcja.h"
#include "instrukcja_set.h"
#include "instrukcja_load.h"
#include "instrukcja_store.h"
#include "instrukcja_add.h"
#include "instrukcja_sub.h"
#include "instrukcja_mul.h"
#include "instrukcja_div.h"
#include "instrukcja_println.h"
