/* Zadanie 6.
 * tk334580 & am347171
 *
 * RRScheduling
 *
 * Każdy proces w systemie dostaje ten sam kwant czasu, po wyczerpaniu
 * którego jest wywłaszczany i następny proces uzyskuje dostęp do procesora.
 * Czas w naszym systemie jest liczony taktami procesora. Każda instrukcja
 * maszynowa zajmuje 1 takt procesora.
 * 
 */

#ifndef RR_SCHEDULING_H
#define RR_SCHEDULING_H

#include <list>

#include "scheduling_algorithm.h"

class RRScheduling : public SchedulingAlgorithm {
    
    public:
        RRScheduling(time_type quantum) : quantum(quantum) {
            if (!quantum)
                throw IllegalArgumentException{};
        }
        
        void setProcesses(std::vector<Proces>& v) {
            // czyszczenie procesów z poprzedniego wykonania
            processes_in_order.clear();
            
            for (size_t i = 0; i < v.size(); ++i)
                processes_in_order.push_back(std::make_pair(i, v[i].getNumber()));
            it = processes_in_order.begin();
        }
        
        std::pair<int, time_type> next() {
            if (processes_in_order.empty())
                return std::make_pair(-1,0);

            std::pair<int, time_type> res;
            
            if (it->second > quantum) {
				// proces ma jeszcze instrukcje do wykonania
                res = std::make_pair(it->first, quantum);
                it->second -= quantum;
                it++;
            } else {
				// proces wykonał się w całości
                res = *it;
                it = processes_in_order.erase(it);
            }

            // przeglądanie cykliczne
            if (it == processes_in_order.end())
                it = processes_in_order.begin();
            
            return res;
        }

    private:
        std::list<std::pair<int, time_type> > processes_in_order;
        // iterator do aktualnego miejsca w liście procesów 
        std::list<std::pair<int, time_type> >::iterator it;
        time_type quantum;
};

#endif // RR_SCHEDULING_H
