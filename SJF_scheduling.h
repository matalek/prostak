/* Zadanie 6.
 * tk334580 & am347171
 *
 * SJFScheduling
 *
 * Do wykonania wybierany jest proces, który najszybciej się skończy.
 */

#ifndef SJFS_SCHEDULING_H
#define SJFS_SCHEDULING_H

#include <algorithm>
#include <vector>

#include "scheduling_algorithm.h"

class SJFScheduling : public SchedulingAlgorithm {
    

    public:
        void setProcesses(std::vector<Proces>& v) {
            // czyszczenie procesów z poprzedniego wykonania        
            processes_in_order.clear();
            cnt = 0;
            
            for (size_t i = 0; i < v.size(); i++)
                processes_in_order.push_back(std::make_pair(i, v[i].getNumber()));
            std::sort(processes_in_order.begin(),processes_in_order.end(),cmp_obj);
                
        }
            
        std::pair<int, time_type> next() {
            if (cnt >= processes_in_order.size())
                return std::make_pair(-1,0);
            return processes_in_order[cnt++];
        }

    private:
        std::vector<std::pair<int, time_type> > processes_in_order;
        size_t cnt; // numer aktualnego miejsca w wektorze procesów

        // pomocnicza struktura do sortowania wektora par względem drugiej współrzędnej
        struct cmp {
            bool operator()(const std::pair<int, time_type> &a, const std::pair<int, time_type> &b) const{
                return (a.second == b.second) ? a.first < b.first : a.second < b.second;
            }
        } cmp_obj;
};

#endif // SJFS_SCHEDULING_H
